# Ricochet Unity Game

Top-down ricochet shooter game developed with the Unity engine in C#. This project is intended as a raycasting demo and is based in part on a game developed by Sebastian Lague. 
