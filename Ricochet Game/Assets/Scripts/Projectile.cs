using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public LayerMask collisionMask;

    public Transform Bullet;
    private float speed = 15;

    // Update is called once per frame
    void Update()
    {
        //move bullet forward
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        //raycasting
        Ray ray = new Ray (transform.position, transform.forward);

        if (Physics.Raycast(ray, out RaycastHit hit, Time.deltaTime * speed + 0.1f, collisionMask))
        {
            Vector3 reflectDir = Vector3.Reflect(ray.direction, hit.normal);
            float rot = 90 - Mathf.Atan2(reflectDir.z, reflectDir.x) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, rot, 0);
        }
    }
}
