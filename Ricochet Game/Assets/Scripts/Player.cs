using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public GameObject Bullet;

    private Camera cam;
    private float speed = 3;
    //public so it shows up in inspector
    public LayerMask mask;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //get cursor position in world coordinates
        Vector3 m = Input.mousePosition;
        m = cam.ScreenToWorldPoint(new Vector3(m.x,m.y,9.5f));
        //rotate player towards cursor
        transform.LookAt(m);
        //move player forwards when there's input on vertical axis
        transform.Translate(Vector3.forward * speed * Time.deltaTime * Input.GetAxisRaw("Vertical"), Space.Self);

        //when LMB pressed, shoot projectile 
        if (Input.GetMouseButtonDown(0)){
            Instantiate(Bullet, transform.position, transform.rotation);
        }

        //raycasting
        Ray ray = new Ray (transform.position, transform.forward);
        RaycastHit hitInfo;

        if (Physics.Raycast (ray, out hitInfo, 100, mask)) {
            Debug.DrawLine (ray.origin, hitInfo.point, Color.red);
        } else {
            Debug.DrawLine (ray.origin, ray.origin + ray.direction * 100, Color.green);
        }

    }
}
